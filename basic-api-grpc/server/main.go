package main

import (
	"context"
	"net"

	"grpcServer/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	DataPost = []proto.PostData{}
)

type PostServiceServer struct {
}

func (s *PostServiceServer) AddPost(ctx context.Context, request *proto.PostData) (*proto.PostResponse, error) {
	result := &proto.PostResponse{}
	DataPost = append(DataPost, (*request))
	result.StatusCode = "00"
	result.StatusMessage = "success"
	return result, nil
}

func (s *PostServiceServer) GetPost(ctx context.Context, request *proto.Empty) (*proto.ListPost, error) {

	result := proto.ListPost{}

	for _, val := range DataPost {
		result.List = append(result.List, &val)
	}

	return &result, nil
}

type server struct {
}

func (s *server) Add(ctx context.Context, request *proto.Request) (*proto.Response, error) {
	a, b := request.GetA(), request.GetB()

	result := a + b

	return &proto.Response{Result: result}, nil
}

func (s *server) Multiply(ctx context.Context, request *proto.Request) (*proto.Response, error) {
	a, b := request.GetA(), request.GetB()

	result := a * b

	return &proto.Response{Result: result}, nil
}

func main() {

	listener, err := net.Listen("tcp", "0.0.0.0:2424")

	if err != nil {
		panic(err)
	}

	srv := grpc.NewServer()

	proto.RegisterAddServiceServer(srv, &server{})
	proto.RegisterPostServiceServer(srv, &PostServiceServer{})

	reflection.Register(srv)

	if e := srv.Serve(listener); e != nil {
		panic(e)
	}

}
