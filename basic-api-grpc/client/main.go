package main

import (
	"fmt"
	"grpcClient/proto"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

func main() {
	conn, err := grpc.Dial("localhost:2424", grpc.WithInsecure())

	if err != nil {
		panic(err)
	}

	client := proto.NewAddServiceClient(conn)
	clientPost := proto.NewPostServiceClient(conn)

	g := gin.Default()

	g.GET("/add/:a/:b", func(ctx *gin.Context) {

		a, err := strconv.ParseUint(ctx.Param("a"), 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter a"})
		}

		b, err := strconv.ParseUint(ctx.Param("b"), 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter b"})
		}

		req := &proto.Request{A: int64(a), B: int64(b)}

		if response, err := client.Add(ctx, req); err == nil {

			ctx.JSON(http.StatusOK, gin.H{
				"result": fmt.Sprint(response.Result),
			})

		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

	})

	g.GET("/mult/:a/:b", func(ctx *gin.Context) {

		a, err := strconv.ParseUint(ctx.Param("a"), 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter a"})
		}

		b, err := strconv.ParseUint(ctx.Param("b"), 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter b"})
		}

		req := &proto.Request{A: int64(a), B: int64(b)}

		if response, err := client.Multiply(ctx, req); err == nil {

			ctx.JSON(http.StatusOK, gin.H{
				"result": fmt.Sprint(response.Result),
			})

		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

	})

	g.POST("/post/add", func(ctx *gin.Context) {

		param := proto.PostData{}

		ctx.BindJSON(&param)

		if response, err := clientPost.AddPost(ctx, &param); err == nil {

			ctx.JSON(http.StatusOK, gin.H{
				"StatusCode":    fmt.Sprint(response.StatusCode),
				"StatusMessage": fmt.Sprint(response.StatusMessage),
			})

		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

	})

	g.GET("/post", func(ctx *gin.Context) {

		if response, err := clientPost.GetPost(ctx, &emptypb.Empty{}); err == nil {

			ctx.JSON(http.StatusOK, gin.H{
				"List": response.List,
			})

		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

	})

	if err := g.Run(":0404"); err != nil {
		log.Fatalf("Failed to run server: %v", err)
	}
}
