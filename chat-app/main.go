package main

import (
	"chatApp/proto"
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"sync"

	"google.golang.org/grpc"
	glog "google.golang.org/grpc/grpclog"
)

var grpcLog glog.LoggerV2

func init() {
	grpcLog = glog.NewLoggerV2(os.Stdout, os.Stdout, os.Stdout)
}

type Connection struct {
	stream proto.Broadcast_CreateStreamServer
	id     string
	active bool
	error  chan error
}

type BroadCastServer struct {
	Connection []*Connection
}

func (s *BroadCastServer) CreateStream(pconn *proto.Connect, stream proto.Broadcast_CreateStreamServer) error {
	conn := &Connection{
		stream: stream,
		id:     pconn.User.Id,
		active: true,
		error:  make(chan error),
	}
	fmt.Printf("Creating connection for %v", pconn.User.Name)
	s.Connection = append(s.Connection, conn)

	return <-conn.error
}

func (s *BroadCastServer) BroadcastMessage(ctx context.Context, msg *proto.Message) (*proto.Close, error) {
	wait := sync.WaitGroup{}
	done := make(chan int)

	for _, conn := range s.Connection {
		wait.Add(1)

		go func(msg *proto.Message, conn *Connection) {

			defer wait.Done()

			if conn.active {
				err := conn.stream.Send(msg)
				grpcLog.Info("Sending message to : ", conn.stream)
				fmt.Println(msg.Content)
				if err != nil {
					grpcLog.Errorf("Error with Stream : %v - Error: %v", conn.stream, err)
					conn.active = false
					conn.error <- err
				}
			}

		}(msg, conn)
	}

	go func() {

		wait.Wait()
		close(done)

	}()

	<-done

	return &proto.Close{}, nil
}

func main() {

	var connection []*Connection

	broadCastServer := &BroadCastServer{connection}

	grpcServer := grpc.NewServer()

	listener, err := net.Listen("tcp", ":2424")

	if err != nil {
		log.Fatalf("error creating server %v", err)
	}

	grpcLog.Info("Starting Server at port :2424")

	proto.RegisterBroadcastServer(grpcServer, broadCastServer)

	grpcServer.Serve(listener)

}
