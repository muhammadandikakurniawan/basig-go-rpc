package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type ParamEditItem struct {
	Item Item
	Id   int
}

type Item struct {
	Title string
	Body  string
}

type API int

var database []Item

func (a *API) GetAll(empty string, reply *[]Item) error {

	*reply = database

	return nil

}

func (a *API) GetByName(Title string, reply *Item) error {

	var getItem Item

	for _, val := range database {

		if val.Title == Title {
			getItem = val
		}

	}

	*reply = getItem

	return nil
}

func (a *API) AddItem(item Item, reply *Item) error {
	database = append(database, item)

	*reply = item

	return nil
}

func (a *API) EditItem(param ParamEditItem, reply *Item) error {

	*reply = param.Item
	database[param.Id] = param.Item

	return nil
}

func (a *API) DeleteItem(title string, reply *Item) error {

	for idx, val := range database {
		if val.Title == title {
			*reply = val
			database = append(database[:idx], database[idx+1:]...)
		}
	}

	return nil
}

func (a *API) DeleteItemById(id int, reply *Item) error {

	*reply = database[id]
	database = append(database[:id], database[id+1:]...)

	return nil
}

func main() {
	api := new(API)
	err := rpc.Register(api)

	if err != nil {
		log.Fatal("error register api : ", err)
	}

	rpc.HandleHTTP()

	listener, err := net.Listen("tcp", ":2424")

	if err != nil {
		log.Fatal("Listener error", err)
	}

	log.Printf("serving rpc on port %d", 2424)

	http.Serve(listener, nil)

	if err != nil {
		log.Fatal("error serving: ", err)
	}
}
