package main

import (
	"fmt"
	"log"
	"net/rpc"
)

type ParamEditItem struct {
	Item Item
	Id   int
}

type Item struct {
	Title string
	Body  string
}

func main() {
	var reply Item
	var datas []Item

	client, err := rpc.DialHTTP("tcp", "localhost:2424")

	if err != nil {
		log.Fatal("Connection error: ", err)
	}

	item1 := Item{"Title one", "body one"}
	item2 := Item{"Title two", "body two"}
	item3 := Item{"Title three", "body three"}
	item4 := Item{"Title four", "body four"}

	//add item
	client.Call("API.AddItem", item1, &reply)
	client.Call("API.AddItem", item2, &reply)
	client.Call("API.AddItem", item3, &reply)
	client.Call("API.AddItem", item4, &reply)

	//get all data
	client.Call("API.GetAll", "", &datas)

	paramEdit := ParamEditItem{Id: 1, Item: Item{"jakarta kebanjiran", "di bogor angin berantem"}}

	// edit data
	client.Call("API.EditItem", paramEdit, &reply)

	//get all data
	client.Call("API.GetAll", "", &datas)

	fmt.Print("Done")
}
